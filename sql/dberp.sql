SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dberp`
--

-- --------------------------------------------------------

--
-- 表的结构 `dberp_accounts_receivable`
--

CREATE TABLE `dberp_accounts_receivable` (
  `receivable_id` int(11) NOT NULL,
  `sales_order_id` int(11) NOT NULL,
  `sales_order_sn` varchar(50) NOT NULL,
  `send_order_id` int(11) NOT NULL,
  `send_order_sn` varchar(50) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `receivable_code` varchar(20) NOT NULL,
  `receivable_amount` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `finish_amount` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `add_time` int(10) NOT NULL,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='应收账款表';

--
-- 表的结构 `dberp_accounts_receivable_log`
--

CREATE TABLE `dberp_accounts_receivable_log` (
  `receivable_log_id` int(11) NOT NULL,
  `receivable_id` int(11) NOT NULL,
  `receivable_log_amount` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `receivable_log_user` varchar(100) NOT NULL,
  `receivable_log_time` int(10) NOT NULL,
  `receivable_file` varchar(255) DEFAULT NULL,
  `receivable_info` varchar(255) DEFAULT NULL,
  `receivable_add_time` int(10) NOT NULL,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='收款记录';

--
-- 表的结构 `dberp_admin`
--

CREATE TABLE `dberp_admin` (
  `admin_id` int(11) NOT NULL,
  `admin_group_id` int(11) NOT NULL,
  `admin_name` varchar(100) NOT NULL,
  `admin_passwd` varchar(72) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `admin_state` tinyint(2) NOT NULL DEFAULT '1',
  `admin_add_time` int(10) NOT NULL,
  `admin_old_login_time` int(10) DEFAULT NULL,
  `admin_new_login_time` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 转存表中的数据 `dberp_admin`
--

INSERT INTO `dberp_admin` (`admin_id`, `admin_group_id`, `admin_name`, `admin_passwd`, `admin_email`, `admin_state`, `admin_add_time`, `admin_old_login_time`, `admin_new_login_time`) VALUES
(1, 1, 'admin', '$2y$10$lbbVRtgz4henw6RlA2NDseERhzpOV6MXo0ty34xsdzy80EpKrudM2', 'admin1@admin.com', 1, 1111111, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `dberp_admin_group`
--

CREATE TABLE `dberp_admin_group` (
  `admin_group_id` int(11) NOT NULL,
  `admin_group_name` varchar(200) NOT NULL,
  `admin_group_purview` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 转存表中的数据 `dberp_admin_group`
--

INSERT INTO `dberp_admin_group` (`admin_group_id`, `admin_group_name`, `admin_group_purview`) VALUES
(1, '管理员', '');

-- --------------------------------------------------------
--
-- 表的结构 `dberp_brand`
--

CREATE TABLE `dberp_brand` (
  `brand_id` int(11) NOT NULL,
  `brand_name` varchar(100) NOT NULL,
  `brand_code` varchar(30) DEFAULT NULL,
  `brand_sort` int(11) NOT NULL DEFAULT '255',
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 表的结构 `dberp_customer`
--

CREATE TABLE `dberp_customer` (
  `customer_id` int(11) NOT NULL,
  `customer_category_id` int(11) NOT NULL,
  `customer_code` varchar(30) NOT NULL,
  `customer_name` varchar(100) NOT NULL,
  `customer_sort` int(11) NOT NULL DEFAULT '255',
  `customer_email` varchar(30) DEFAULT NULL,
  `customer_address` varchar(255) DEFAULT NULL,
  `customer_contacts` varchar(30) DEFAULT NULL,
  `customer_phone` varchar(20) DEFAULT NULL,
  `customer_telephone` varchar(20) DEFAULT NULL,
  `customer_bank` varchar(100) DEFAULT NULL,
  `customer_bank_account` varchar(30) DEFAULT NULL,
  `customer_tax` varchar(30) DEFAULT NULL,
  `customer_info` varchar(255) DEFAULT NULL,
  `admin_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL DEFAULT '0',
  `region_values` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='客户表';

--
-- 表的结构 `dberp_customer_category`
--

CREATE TABLE `dberp_customer_category` (
  `customer_category_id` int(11) NOT NULL,
  `customer_category_code` varchar(30) NOT NULL,
  `customer_category_name` varchar(100) NOT NULL,
  `customer_category_sort` int(11) NOT NULL DEFAULT '255',
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='客户分类';

--
-- 表的结构 `dberp_finance_payable`
--

CREATE TABLE `dberp_finance_payable` (
  `payable_id` int(11) NOT NULL,
  `warehouse_order_id` int(11) NOT NULL COMMENT '入库单号',
  `p_order_id` int(11) NOT NULL COMMENT '采购订单id',
  `p_order_sn` varchar(50) NOT NULL COMMENT '采购订单号',
  `supplier_id` int(11) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `payment_code` varchar(20) NOT NULL COMMENT '支付方式code',
  `payment_amount` decimal(19,4) NOT NULL DEFAULT '0.0000' COMMENT '采购支付金额',
  `finish_amount` decimal(19,4) DEFAULT '0.0000' COMMENT '采购已经支付金额',
  `add_time` int(10) NOT NULL COMMENT '添加时间',
  `admin_id` int(11) NOT NULL COMMENT '管理员id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 表的结构 `dberp_finance_payable_log`
--

CREATE TABLE `dberp_finance_payable_log` (
  `pay_log_id` int(11) NOT NULL,
  `payable_id` int(11) NOT NULL COMMENT '应付款账单id',
  `pay_log_amount` decimal(19,4) DEFAULT '0.0000' COMMENT '付款金额',
  `pay_log_user` varchar(100) NOT NULL COMMENT '付款人姓名',
  `pay_log_paytime` int(10) NOT NULL COMMENT '付款时间',
  `pay_file` varchar(255) DEFAULT NULL,
  `pay_log_info` varchar(255) DEFAULT NULL COMMENT '付款备注信息',
  `pay_log_addtime` int(10) NOT NULL COMMENT '记录添加时间',
  `admin_id` int(11) NOT NULL COMMENT '操作者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 表的结构 `dberp_goods`
--

CREATE TABLE `dberp_goods` (
  `goods_id` int(11) NOT NULL,
  `goods_category_id` int(11) NOT NULL,
  `brand_id` int(11) DEFAULT '0',
  `goods_name` varchar(100) NOT NULL,
  `goods_stock` int(11) DEFAULT '0',
  `goods_spec` varchar(100) DEFAULT NULL,
  `goods_number` varchar(30) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `goods_barcode` varchar(30) DEFAULT NULL,
  `goods_info` varchar(500) DEFAULT NULL,
  `goods_sort` int(11) NOT NULL DEFAULT '255',
  `goods_price` decimal(19,4) DEFAULT '0.0000',
  `admin_id` int(11) NOT NULL,
  `goods_recommend_price` decimal(19,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='基础商品表';

--
-- 表的结构 `dberp_goods_category`
--

CREATE TABLE `dberp_goods_category` (
  `goods_category_id` int(11) NOT NULL,
  `goods_category_top_id` int(11) NOT NULL DEFAULT '0',
  `goods_category_code` varchar(30) NOT NULL,
  `goods_category_name` varchar(100) NOT NULL,
  `goods_category_path` varchar(255) DEFAULT '0',
  `goods_category_sort` int(11) NOT NULL DEFAULT '255',
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 表的结构 `dberp_operlog`
--

CREATE TABLE `dberp_operlog` (
  `log_id` int(11) NOT NULL,
  `log_oper_user` varchar(100) NOT NULL,
  `log_oper_user_group` varchar(100) NOT NULL,
  `log_time` int(10) NOT NULL,
  `log_ip` varchar(50) NOT NULL,
  `log_body` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='操作记录';

--
-- 表的结构 `dberp_position`
--

CREATE TABLE `dberp_position` (
  `position_id` int(11) NOT NULL,
  `position_sn` varchar(30) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='仓库仓位';

--
-- 表的结构 `dberp_purchase_goods_price_log`
--

CREATE TABLE `dberp_purchase_goods_price_log` (
  `price_log_id` int(11) NOT NULL,
  `goods_id` int(11) NOT NULL,
  `goods_price` decimal(19,0) NOT NULL,
  `p_order_id` int(11) NOT NULL,
  `log_time` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='采购商品价格历史记录';

--
-- 表的结构 `dberp_purchase_oper_log`
--

CREATE TABLE `dberp_purchase_oper_log` (
  `oper_log_id` int(11) NOT NULL,
  `p_order_id` int(11) NOT NULL,
  `order_state` tinyint(2) NOT NULL,
  `oper_user_id` int(11) NOT NULL,
  `oper_user` varchar(100) NOT NULL,
  `oper_time` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='采购操作记录';

-- --------------------------------------------------------

--
-- 表的结构 `dberp_purchase_order`
--

CREATE TABLE `dberp_purchase_order` (
  `p_order_id` int(11) NOT NULL COMMENT '采购单id',
  `p_order_sn` varchar(50) NOT NULL COMMENT '采购单编号',
  `supplier_id` int(11) NOT NULL COMMENT '供应商id',
  `supplier_contacts` varchar(30) NOT NULL COMMENT '供应商联系人',
  `supplier_phone` varchar(20) DEFAULT NULL COMMENT '手机号码',
  `supplier_telephone` varchar(20) DEFAULT NULL COMMENT '座机号码',
  `p_order_goods_amount` decimal(19,4) DEFAULT '0.0000' COMMENT '商品总额',
  `p_order_tax_amount` decimal(19,4) DEFAULT '0.0000' COMMENT '税金总额',
  `p_order_amount` decimal(19,4) DEFAULT '0.0000' COMMENT '订单总额',
  `p_order_info` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `p_order_state` tinyint(4) DEFAULT '0' COMMENT '采购单状态，0 未审核，1 已审核，2 已入库，-1 退货，-2 退货完成',
  `payment_code` varchar(20) NOT NULL,
  `return_state` tinyint(2) DEFAULT '0',
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 表的结构 `dberp_purchase_order_goods`
--

CREATE TABLE `dberp_purchase_order_goods` (
  `p_goods_id` int(11) NOT NULL COMMENT '采购单商品id',
  `p_order_id` int(11) NOT NULL COMMENT '采购单id',
  `goods_id` int(11) NOT NULL COMMENT '商品id',
  `goods_name` varchar(100) NOT NULL COMMENT '商品名称',
  `goods_number` varchar(30) NOT NULL COMMENT '商品编号',
  `goods_spec` varchar(100) DEFAULT NULL COMMENT '商品规格',
  `goods_unit` varchar(20) DEFAULT NULL COMMENT '商品单位，非对应id，单位名称',
  `p_goods_buy_num` int(11) NOT NULL COMMENT '商品购买数量',
  `p_goods_price` decimal(19,4) NOT NULL DEFAULT '0.0000' COMMENT '商品购买的单价',
  `p_goods_tax` decimal(19,4) NOT NULL DEFAULT '0.0000' COMMENT '商品税金',
  `p_goods_amount` decimal(19,4) NOT NULL DEFAULT '0.0000' COMMENT '商品总金额',
  `p_goods_info` varchar(255) DEFAULT NULL COMMENT '商品备注'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 表的结构 `dberp_purchase_order_goods_return`
--

CREATE TABLE `dberp_purchase_order_goods_return` (
  `goods_return_id` int(11) NOT NULL COMMENT '退货商品id',
  `order_return_id` int(11) NOT NULL COMMENT '退货单id',
  `p_goods_id` int(11) NOT NULL COMMENT '采购商品id',
  `goods_name` varchar(100) NOT NULL COMMENT '商品名称',
  `goods_number` varchar(50) NOT NULL COMMENT '商品编号',
  `goods_spec` varchar(100) DEFAULT NULL COMMENT '商品规格',
  `goods_unit` varchar(20) NOT NULL COMMENT '商品单位',
  `p_goods_price` decimal(19,4) NOT NULL DEFAULT '0.0000' COMMENT '单品采购价',
  `p_goods_tax` decimal(19,4) NOT NULL DEFAULT '0.0000' COMMENT '税费',
  `goods_return_num` int(11) NOT NULL DEFAULT '0' COMMENT '退货数量',
  `goods_return_amount` decimal(19,4) NOT NULL DEFAULT '0.0000' COMMENT '退货金额'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='退货商品';

--
-- 表的结构 `dberp_purchase_order_return`
--

CREATE TABLE `dberp_purchase_order_return` (
  `order_return_id` int(11) NOT NULL COMMENT '退货单id',
  `p_order_id` int(11) NOT NULL COMMENT '采购订单id',
  `p_order_sn` varchar(50) NOT NULL COMMENT '采购单编号',
  `p_order_goods_return_amount` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `p_order_return_amount` decimal(19,4) NOT NULL DEFAULT '0.0000' COMMENT '退货单金额',
  `p_order_return_info` varchar(500) DEFAULT NULL COMMENT '退货原因',
  `return_time` int(10) NOT NULL COMMENT '退货单添加时间',
  `return_state` tinyint(2) NOT NULL DEFAULT '-1',
  `return_finish_time` int(10) DEFAULT NULL,
  `admin_id` int(11) NOT NULL COMMENT '操作者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='退货单';

--
-- 表的结构 `dberp_purchase_warehouse_order`
--

CREATE TABLE `dberp_purchase_warehouse_order` (
  `warehouse_order_id` int(11) NOT NULL,
  `p_order_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `warehouse_order_sn` varchar(50) NOT NULL,
  `warehouse_order_state` tinyint(1) DEFAULT '2',
  `warehouse_order_info` varchar(255) DEFAULT NULL,
  `admin_id` int(11) NOT NULL,
  `warehouse_order_goods_amount` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `warehouse_order_tax` decimal(19,4) DEFAULT '0.0000',
  `warehouse_order_amount` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `warehouse_order_payment_code` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 表的结构 `dberp_purchase_warehouse_order_goods`
--

CREATE TABLE `dberp_purchase_warehouse_order_goods` (
  `warehouse_order_goods_id` int(11) NOT NULL,
  `warehouse_order_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `p_order_id` int(11) NOT NULL,
  `warehouse_goods_buy_num` int(11) DEFAULT '0',
  `warehouse_goods_price` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `warehouse_goods_tax` decimal(19,4) DEFAULT '0.0000',
  `warehouse_goods_amount` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `goods_id` int(11) NOT NULL,
  `goods_name` varchar(100) NOT NULL,
  `goods_number` varchar(30) NOT NULL,
  `goods_spec` varchar(100) DEFAULT NULL,
  `goods_unit` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 表的结构 `dberp_region`
--

CREATE TABLE `dberp_region` (
  `region_id` int(11) NOT NULL,
  `region_name` varchar(50) NOT NULL,
  `region_top_id` int(11) NOT NULL DEFAULT '0',
  `region_sort` int(11) NOT NULL DEFAULT '255',
  `region_path` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='地区数据表';

--
-- 表的结构 `dberp_sales_goods_price_log`
--

CREATE TABLE `dberp_sales_goods_price_log` (
  `price_log_id` int(11) NOT NULL,
  `goods_id` int(11) NOT NULL,
  `goods_price` decimal(19,0) NOT NULL,
  `sales_order_id` int(11) NOT NULL,
  `log_time` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='销售商品价格历史记录';

--
-- 表的结构 `dberp_sales_oper_log`
--

CREATE TABLE `dberp_sales_oper_log` (
  `oper_log_id` int(11) NOT NULL,
  `sales_order_id` int(11) NOT NULL,
  `order_state` tinyint(2) NOT NULL,
  `oper_user_id` int(11) NOT NULL,
  `oper_user` varchar(100) NOT NULL,
  `oper_time` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='销售操作记录';

-- --------------------------------------------------------

--
-- 表的结构 `dberp_sales_order`
--

CREATE TABLE `dberp_sales_order` (
  `sales_order_id` int(11) NOT NULL COMMENT '销售订单id',
  `sales_order_sn` varchar(50) NOT NULL COMMENT '销售订单编号',
  `customer_id` int(11) NOT NULL COMMENT '客户id',
  `customer_contacts` varchar(30) NOT NULL COMMENT '客户联系人',
  `customer_address` varchar(255) NOT NULL,
  `customer_phone` varchar(20) DEFAULT NULL COMMENT '客户手机',
  `customer_telephone` varchar(20) DEFAULT NULL COMMENT '客户电话',
  `sales_order_goods_amount` decimal(19,4) NOT NULL DEFAULT '0.0000' COMMENT '客户购买商品金额',
  `sales_order_tax_amount` decimal(19,4) NOT NULL DEFAULT '0.0000' COMMENT '商品税金',
  `sales_order_amount` decimal(19,4) NOT NULL DEFAULT '0.0000' COMMENT '客户购买商品总额',
  `receivables_code` varchar(20) NOT NULL COMMENT '支付方式',
  `sales_order_state` tinyint(4) NOT NULL DEFAULT '0' COMMENT '销售状态',
  `sales_order_info` varchar(500) DEFAULT NULL COMMENT '备注说明',
  `return_state` tinyint(2) NOT NULL DEFAULT '0' COMMENT '退货状态',
  `admin_id` int(11) NOT NULL COMMENT '操作者id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='销售订单';

--
-- 表的结构 `dberp_sales_order_goods`
--

CREATE TABLE `dberp_sales_order_goods` (
  `sales_goods_id` int(11) NOT NULL COMMENT '销售商品id',
  `sales_order_id` int(11) NOT NULL COMMENT '销售订单id',
  `goods_id` int(11) NOT NULL COMMENT '商品id',
  `goods_name` varchar(100) NOT NULL COMMENT '商品名称',
  `goods_number` varchar(30) NOT NULL COMMENT '商品编号',
  `goods_spec` varchar(100) DEFAULT NULL COMMENT '商品规格',
  `goods_unit` varchar(20) NOT NULL COMMENT '商品单位',
  `sales_goods_sell_num` int(11) NOT NULL COMMENT '销售商品数量',
  `sales_goods_price` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `sales_goods_tax` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `sales_goods_amount` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `sales_goods_info` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='销售商品表';

--
-- 表的结构 `dberp_sales_order_goods_return`
--

CREATE TABLE `dberp_sales_order_goods_return` (
  `goods_return_id` int(11) NOT NULL,
  `sales_order_return_id` int(11) NOT NULL,
  `sales_goods_id` int(11) NOT NULL,
  `goods_name` varchar(100) NOT NULL,
  `goods_number` varchar(50) NOT NULL,
  `goods_spec` varchar(100) DEFAULT NULL,
  `goods_unit` varchar(20) NOT NULL,
  `sales_goods_price` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `sales_goods_tax` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `goods_return_num` int(11) NOT NULL DEFAULT '0',
  `goods_return_amount` decimal(19,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='销售退货商品表';

--
-- 表的结构 `dberp_sales_order_return`
--

CREATE TABLE `dberp_sales_order_return` (
  `sales_order_return_id` int(11) NOT NULL,
  `sales_order_id` int(11) NOT NULL,
  `sales_order_sn` varchar(50) NOT NULL,
  `sales_send_order_id` int(11) NOT NULL,
  `sales_send_order_sn` varchar(50) NOT NULL,
  `sales_order_goods_return_amount` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `sales_order_return_amount` decimal(19,4) NOT NULL DEFAULT '0.0000',
  `sales_order_return_info` varchar(500) DEFAULT NULL,
  `return_time` int(10) NOT NULL,
  `return_state` tinyint(2) NOT NULL,
  `return_finish_time` int(10) DEFAULT NULL,
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='销售退货单表';

--
-- 表的结构 `dberp_sales_send_order`
--

CREATE TABLE `dberp_sales_send_order` (
  `send_order_id` int(11) NOT NULL,
  `send_order_sn` varchar(50) NOT NULL,
  `sales_order_id` int(11) NOT NULL,
  `return_state` tinyint(1) NOT NULL DEFAULT '0',
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单发货表';

--
-- 表的结构 `dberp_sales_send_warehouse_goods`
--

CREATE TABLE `dberp_sales_send_warehouse_goods` (
  `send_warehouse_goods_id` int(11) NOT NULL,
  `goods_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `send_goods_stock` int(11) NOT NULL DEFAULT '0',
  `send_order_id` int(11) NOT NULL,
  `sales_order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单发货仓库商品出库表';

--
-- 表的结构 `dberp_supplier`
--

CREATE TABLE `dberp_supplier` (
  `supplier_id` int(11) NOT NULL,
  `supplier_category_id` int(11) NOT NULL,
  `supplier_code` varchar(30) NOT NULL,
  `supplier_name` varchar(100) NOT NULL,
  `supplier_sort` int(11) NOT NULL DEFAULT '255',
  `supplier_address` varchar(255) DEFAULT NULL,
  `supplier_contacts` varchar(30) DEFAULT NULL,
  `supplier_phone` varchar(20) DEFAULT NULL,
  `supplier_telephone` varchar(20) DEFAULT NULL,
  `supplier_bank` varchar(100) DEFAULT NULL,
  `supplier_bank_account` varchar(30) DEFAULT NULL,
  `supplier_tax` varchar(30) DEFAULT NULL,
  `supplier_email` varchar(30) DEFAULT NULL,
  `supplier_info` varchar(255) DEFAULT NULL,
  `admin_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL DEFAULT '0',
  `region_values` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 表的结构 `dberp_supplier_category`
--

CREATE TABLE `dberp_supplier_category` (
  `supplier_category_id` int(11) NOT NULL,
  `supplier_category_code` varchar(30) NOT NULL,
  `supplier_category_name` varchar(100) NOT NULL,
  `supplier_category_sort` int(11) NOT NULL DEFAULT '255',
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 表的结构 `dberp_unit`
--

CREATE TABLE `dberp_unit` (
  `unit_id` int(11) NOT NULL,
  `unit_name` varchar(50) NOT NULL,
  `unit_sort` int(11) NOT NULL DEFAULT '255',
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='计量单位';

-- --------------------------------------------------------

--
-- 表的结构 `dberp_warehouse`
--

CREATE TABLE `dberp_warehouse` (
  `warehouse_id` int(11) NOT NULL,
  `warehouse_sn` varchar(30) NOT NULL,
  `warehouse_name` varchar(100) NOT NULL,
  `warehouse_contacts` varchar(50) DEFAULT NULL,
  `warehouse_phone` varchar(30) DEFAULT NULL,
  `warehouse_sort` int(11) NOT NULL DEFAULT '255',
  `admin_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='仓库数据表';

-- --------------------------------------------------------

--
-- 表的结构 `dberp_warehouse_goods`
--

CREATE TABLE `dberp_warehouse_goods` (
  `warehouse_goods_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `goods_id` int(11) NOT NULL,
  `warehouse_goods_stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 表的结构 `dberp_system`
--

CREATE TABLE `dberp_system` (
  `sys_id` int(11) NOT NULL,
  `sys_name` varchar(30) NOT NULL,
  `sys_body` text,
  `sys_type` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统信息表';

--
-- 转存表中的数据 `dberp_system`
--

INSERT INTO `dberp_system` (`sys_id`, `sys_name`, `sys_body`, `sys_type`) VALUES
(1, 'company_name', '北京珑大钜商科技有限公司', 'base');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dberp_accounts_receivable`
--
ALTER TABLE `dberp_accounts_receivable`
  ADD PRIMARY KEY (`receivable_id`),
  ADD KEY `receivable_index` (`sales_order_id`,`send_order_id`,`customer_id`,`add_time`,`admin_id`);

--
-- Indexes for table `dberp_accounts_receivable_log`
--
ALTER TABLE `dberp_accounts_receivable_log`
  ADD PRIMARY KEY (`receivable_log_id`),
  ADD KEY `receivable_log_index` (`receivable_id`,`receivable_add_time`,`receivable_log_time`,`admin_id`);

--
-- Indexes for table `dberp_admin`
--
ALTER TABLE `dberp_admin`
  ADD PRIMARY KEY (`admin_id`),
  ADD KEY `admin_name` (`admin_name`,`admin_email`),
  ADD KEY `admin_group_id` (`admin_group_id`),
  ADD KEY `admin_state` (`admin_state`);

--
-- Indexes for table `dberp_admin_group`
--
ALTER TABLE `dberp_admin_group`
  ADD PRIMARY KEY (`admin_group_id`);

--
-- Indexes for table `dberp_brand`
--
ALTER TABLE `dberp_brand`
  ADD PRIMARY KEY (`brand_id`),
  ADD KEY `dberp_brand_index` (`brand_name`,`brand_code`,`brand_sort`);

--
-- Indexes for table `dberp_customer`
--
ALTER TABLE `dberp_customer`
  ADD PRIMARY KEY (`customer_id`),
  ADD KEY `dberp_customer_index` (`customer_code`,`customer_sort`,`customer_name`);

--
-- Indexes for table `dberp_customer_category`
--
ALTER TABLE `dberp_customer_category`
  ADD PRIMARY KEY (`customer_category_id`),
  ADD KEY `dberp_customer_category_index` (`customer_category_code`,`customer_category_name`,`customer_category_sort`);

--
-- Indexes for table `dberp_finance_payable`
--
ALTER TABLE `dberp_finance_payable`
  ADD PRIMARY KEY (`payable_id`),
  ADD KEY `dberp_finance_payment_index` (`warehouse_order_id`,`p_order_id`,`supplier_id`,`payment_code`,`admin_id`,`add_time`);

--
-- Indexes for table `dberp_finance_payable_log`
--
ALTER TABLE `dberp_finance_payable_log`
  ADD PRIMARY KEY (`pay_log_id`),
  ADD KEY `dberp_finance_payable_log_index` (`pay_log_user`,`pay_log_paytime`,`pay_log_addtime`,`payable_id`);

--
-- Indexes for table `dberp_goods`
--
ALTER TABLE `dberp_goods`
  ADD PRIMARY KEY (`goods_id`),
  ADD KEY `dberp_goods_index` (`goods_name`,`brand_id`,`goods_spec`,`goods_category_id`,`goods_sort`);

--
-- Indexes for table `dberp_goods_category`
--
ALTER TABLE `dberp_goods_category`
  ADD PRIMARY KEY (`goods_category_id`),
  ADD KEY `dberp_goods_category_index` (`goods_category_code`,`goods_category_name`,`goods_category_sort`,`goods_category_top_id`);

--
-- Indexes for table `dberp_operlog`
--
ALTER TABLE `dberp_operlog`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `dberp_oper_log_index` (`log_oper_user`,`log_oper_user_group`,`log_time`);

--
-- Indexes for table `dberp_position`
--
ALTER TABLE `dberp_position`
  ADD PRIMARY KEY (`position_id`),
  ADD KEY `position_sn` (`position_sn`,`warehouse_id`,`admin_id`);

--
-- Indexes for table `dberp_purchase_goods_price_log`
--
ALTER TABLE `dberp_purchase_goods_price_log`
  ADD PRIMARY KEY (`price_log_id`),
  ADD KEY `purchase_price_log_index` (`goods_id`,`goods_price`,`p_order_id`,`log_time`);

--
-- Indexes for table `dberp_purchase_oper_log`
--
ALTER TABLE `dberp_purchase_oper_log`
  ADD PRIMARY KEY (`oper_log_id`),
  ADD KEY `p_oper_log` (`p_order_id`,`order_state`,`oper_time`),
  ADD KEY `oper_user_id` (`oper_user_id`);

--
-- Indexes for table `dberp_purchase_order`
--
ALTER TABLE `dberp_purchase_order`
  ADD PRIMARY KEY (`p_order_id`),
  ADD KEY `dberp_purchase_order_index` (`p_order_sn`,`p_order_state`,`supplier_id`);

--
-- Indexes for table `dberp_purchase_order_goods`
--
ALTER TABLE `dberp_purchase_order_goods`
  ADD PRIMARY KEY (`p_goods_id`),
  ADD KEY `dberp_purchase_order_goods_index` (`goods_number`,`goods_name`,`goods_unit`);

--
-- Indexes for table `dberp_purchase_order_goods_return`
--
ALTER TABLE `dberp_purchase_order_goods_return`
  ADD PRIMARY KEY (`goods_return_id`);

--
-- Indexes for table `dberp_purchase_order_return`
--
ALTER TABLE `dberp_purchase_order_return`
  ADD PRIMARY KEY (`order_return_id`),
  ADD KEY `dberp_purchase_order_return_index` (`p_order_id`,`p_order_sn`,`return_time`,`p_order_return_amount`),
  ADD KEY `return_state` (`return_state`,`return_finish_time`);

--
-- Indexes for table `dberp_purchase_warehouse_order`
--
ALTER TABLE `dberp_purchase_warehouse_order`
  ADD PRIMARY KEY (`warehouse_order_id`);

--
-- Indexes for table `dberp_purchase_warehouse_order_goods`
--
ALTER TABLE `dberp_purchase_warehouse_order_goods`
  ADD PRIMARY KEY (`warehouse_order_goods_id`),
  ADD KEY `warehouse_id` (`warehouse_id`);

--
-- Indexes for table `dberp_region`
--
ALTER TABLE `dberp_region`
  ADD PRIMARY KEY (`region_id`),
  ADD KEY `dberp_region_index` (`region_name`,`region_sort`,`region_top_id`);

--
-- Indexes for table `dberp_sales_goods_price_log`
--
ALTER TABLE `dberp_sales_goods_price_log`
  ADD PRIMARY KEY (`price_log_id`),
  ADD KEY `sales_price_log_index` (`goods_id`,`sales_order_id`,`log_time`);

--
-- Indexes for table `dberp_sales_oper_log`
--
ALTER TABLE `dberp_sales_oper_log`
  ADD PRIMARY KEY (`oper_log_id`),
  ADD KEY `s_oper_log_index` (`sales_order_id`,`order_state`,`oper_user_id`,`oper_time`);

--
-- Indexes for table `dberp_sales_order`
--
ALTER TABLE `dberp_sales_order`
  ADD PRIMARY KEY (`sales_order_id`),
  ADD KEY `dberp_sales_order_index` (`sales_order_sn`,`customer_id`,`receivables_code`,`sales_order_state`,`admin_id`);

--
-- Indexes for table `dberp_sales_order_goods`
--
ALTER TABLE `dberp_sales_order_goods`
  ADD PRIMARY KEY (`sales_goods_id`);

--
-- Indexes for table `dberp_sales_order_goods_return`
--
ALTER TABLE `dberp_sales_order_goods_return`
  ADD PRIMARY KEY (`goods_return_id`),
  ADD KEY `sales_order_goods_return_index` (`sales_order_return_id`,`sales_goods_id`);

--
-- Indexes for table `dberp_sales_order_return`
--
ALTER TABLE `dberp_sales_order_return`
  ADD PRIMARY KEY (`sales_order_return_id`),
  ADD KEY `sales_order_return_index` (`sales_order_id`,`sales_order_sn`,`sales_send_order_id`,`sales_send_order_sn`,`admin_id`);

--
-- Indexes for table `dberp_sales_send_order`
--
ALTER TABLE `dberp_sales_send_order`
  ADD PRIMARY KEY (`send_order_id`),
  ADD KEY `send_order_index` (`sales_order_id`,`admin_id`);

--
-- Indexes for table `dberp_sales_send_warehouse_goods`
--
ALTER TABLE `dberp_sales_send_warehouse_goods`
  ADD PRIMARY KEY (`send_warehouse_goods_id`),
  ADD KEY `send_warehouse_goods_index` (`goods_id`,`warehouse_id`,`send_goods_stock`,`send_order_id`,`sales_order_id`);

--
-- Indexes for table `dberp_supplier`
--
ALTER TABLE `dberp_supplier`
  ADD PRIMARY KEY (`supplier_id`),
  ADD KEY `dberp_supplier_index` (`supplier_name`,`supplier_code`,`supplier_sort`);

--
-- Indexes for table `dberp_supplier_category`
--
ALTER TABLE `dberp_supplier_category`
  ADD PRIMARY KEY (`supplier_category_id`),
  ADD KEY `dberp_supplier_category_index` (`supplier_category_code`,`supplier_category_name`,`supplier_category_sort`,`admin_id`);

--
-- Indexes for table `dberp_unit`
--
ALTER TABLE `dberp_unit`
  ADD PRIMARY KEY (`unit_id`),
  ADD KEY `unit_name` (`unit_name`,`admin_id`),
  ADD KEY `dberp_unit_unit_sort_index` (`unit_sort`);

--
-- Indexes for table `dberp_warehouse`
--
ALTER TABLE `dberp_warehouse`
  ADD PRIMARY KEY (`warehouse_id`),
  ADD KEY `warehouse_sn` (`warehouse_sn`,`warehouse_name`,`admin_id`),
  ADD KEY `waerehouse_sort` (`warehouse_sort`);

--
-- Indexes for table `dberp_warehouse_goods`
--
ALTER TABLE `dberp_warehouse_goods`
  ADD PRIMARY KEY (`warehouse_goods_id`),
  ADD KEY `dberp_warehouse_goods_index` (`warehouse_id`,`goods_id`,`warehouse_goods_stock`);

--
-- Indexes for table `dberp_system`
--
ALTER TABLE `dberp_system`
  ADD PRIMARY KEY (`sys_id`),
  ADD KEY `dberp_sys_index` (`sys_name`,`sys_type`);

--
-- 使用表AUTO_INCREMENT `dberp_accounts_receivable`
--
ALTER TABLE `dberp_accounts_receivable`
  MODIFY `receivable_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_accounts_receivable_log`
--
ALTER TABLE `dberp_accounts_receivable_log`
  MODIFY `receivable_log_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_admin`
--
ALTER TABLE `dberp_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_admin_group`
--
ALTER TABLE `dberp_admin_group`
  MODIFY `admin_group_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_brand`
--
ALTER TABLE `dberp_brand`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_customer`
--
ALTER TABLE `dberp_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_customer_category`
--
ALTER TABLE `dberp_customer_category`
  MODIFY `customer_category_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_finance_payable`
--
ALTER TABLE `dberp_finance_payable`
  MODIFY `payable_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_finance_payable_log`
--
ALTER TABLE `dberp_finance_payable_log`
  MODIFY `pay_log_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_goods`
--
ALTER TABLE `dberp_goods`
  MODIFY `goods_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_goods_category`
--
ALTER TABLE `dberp_goods_category`
  MODIFY `goods_category_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_operlog`
--
ALTER TABLE `dberp_operlog`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_position`
--
ALTER TABLE `dberp_position`
  MODIFY `position_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_purchase_goods_price_log`
--
ALTER TABLE `dberp_purchase_goods_price_log`
  MODIFY `price_log_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_purchase_oper_log`
--
ALTER TABLE `dberp_purchase_oper_log`
  MODIFY `oper_log_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_purchase_order`
--
ALTER TABLE `dberp_purchase_order`
  MODIFY `p_order_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '采购单id';
--
-- 使用表AUTO_INCREMENT `dberp_purchase_order_goods`
--
ALTER TABLE `dberp_purchase_order_goods`
  MODIFY `p_goods_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '采购单商品id';
--
-- 使用表AUTO_INCREMENT `dberp_purchase_order_goods_return`
--
ALTER TABLE `dberp_purchase_order_goods_return`
  MODIFY `goods_return_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '退货商品id';
--
-- 使用表AUTO_INCREMENT `dberp_purchase_order_return`
--
ALTER TABLE `dberp_purchase_order_return`
  MODIFY `order_return_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '退货单id';
--
-- 使用表AUTO_INCREMENT `dberp_purchase_warehouse_order`
--
ALTER TABLE `dberp_purchase_warehouse_order`
  MODIFY `warehouse_order_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_purchase_warehouse_order_goods`
--
ALTER TABLE `dberp_purchase_warehouse_order_goods`
  MODIFY `warehouse_order_goods_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_region`
--
ALTER TABLE `dberp_region`
  MODIFY `region_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_sales_goods_price_log`
--
ALTER TABLE `dberp_sales_goods_price_log`
  MODIFY `price_log_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_sales_oper_log`
--
ALTER TABLE `dberp_sales_oper_log`
  MODIFY `oper_log_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_sales_order`
--
ALTER TABLE `dberp_sales_order`
  MODIFY `sales_order_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '销售订单id';
--
-- 使用表AUTO_INCREMENT `dberp_sales_order_goods`
--
ALTER TABLE `dberp_sales_order_goods`
  MODIFY `sales_goods_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '销售商品id';
--
-- 使用表AUTO_INCREMENT `dberp_sales_order_goods_return`
--
ALTER TABLE `dberp_sales_order_goods_return`
  MODIFY `goods_return_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_sales_order_return`
--
ALTER TABLE `dberp_sales_order_return`
  MODIFY `sales_order_return_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_sales_send_order`
--
ALTER TABLE `dberp_sales_send_order`
  MODIFY `send_order_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_sales_send_warehouse_goods`
--
ALTER TABLE `dberp_sales_send_warehouse_goods`
  MODIFY `send_warehouse_goods_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_supplier`
--
ALTER TABLE `dberp_supplier`
  MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_supplier_category`
--
ALTER TABLE `dberp_supplier_category`
  MODIFY `supplier_category_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_unit`
--
ALTER TABLE `dberp_unit`
  MODIFY `unit_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_warehouse`
--
ALTER TABLE `dberp_warehouse`
  MODIFY `warehouse_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `dberp_warehouse_goods`
--
ALTER TABLE `dberp_warehouse_goods`
  MODIFY `warehouse_goods_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `dberp_system`
--
ALTER TABLE `dberp_system`
  MODIFY `sys_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;